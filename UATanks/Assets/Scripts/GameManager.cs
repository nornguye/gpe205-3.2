﻿//Norman Nguyen
//Game Manager: Where it manages your important variables like scores and players, but didn't used it for this milestone yet.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public TankController player;

    //Prefabs
    public GameObject playerPrefab;
    //How many AI tanks
    public int numAITanks;
    //Enemy Prefab
    public List<GameObject> enemyPrefabs;

    //Map Generator
    public MapGenerator mapGenerator;

	// Use this for initialization 
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            {
                Debug.LogError("ERROR: There can be only be one GameManager.");
                Destroy(gameObject);
            }
        }
    }
    //Start
    void Start()
    {
        //Start to the creation of the player tank and AI
        makePlayerTank();
        makeEnemyTank();
    }
    //Make Player Tank
    public void makePlayerTank()
    {
        //Generate the Tank
        //Vector 3 with Random Range where the tank will spawn randomly within that range
        Vector3 position = new Vector3(Random.Range(-50.0f, 50.0f), -5, Random.Range(-50.0f, 50.0f));
        GameObject playerSpawnObject = Instantiate(playerPrefab, position, Quaternion.identity) as GameObject;
        playerSpawnObject.transform.parent = this.transform;
    }
    //Random Enemy Prefab
    public GameObject RandomEnemyPrefab()
    {
        return enemyPrefabs[Random.Range(0, enemyPrefabs.Count)];
    }
    //Make AI Tanks
    public void makeEnemyTank()
    {
        //For Loop
        for (int enemyTanks = 0; enemyTanks < numAITanks; enemyTanks++)
        {
            //Vector3 Position for the tank ranging withint -60 to 60 in X and Z
            Vector3 position = new Vector3(Random.Range(-60.0f, 60.0f), -5, Random.Range(-60.0f, 60.0f));
            //Spawn the object based on the enemies listed
            GameObject enemySpawnedObject = Instantiate(RandomEnemyPrefab(), position, Quaternion.identity) as GameObject;
            enemySpawnedObject.transform.parent = this.transform;
            //Name of the Enemy Tank
            enemySpawnedObject.name = "Enemy Tank ("+enemyTanks+")";
        }
    
        
    }
}
