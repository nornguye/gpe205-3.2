﻿//Norman Nguyen

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    //Rows
    public int rows;
    //Columns
    public int cols;
    //Width of the room
    public float roomWidth = 60.0f;
    //Height of the room
    public float roomHeight = 60.0f;
    //Room Game Object List
    public List<GameObject> roomPrefab;
    //Seed
    public int seed;
    //Room Grid
    private Room[,] grid;
    //enum list of map types
    public bool mapOfTheDay;
    public bool randomMap;
    public bool seededMap;

    //Start the script to generate the map
    void Start()
    {
        GenerateMap();
    }
    //Random Room Prefab as spawning random rooms
    public GameObject RandomRoomPrefab()
    {
        return roomPrefab[UnityEngine.Random.Range(0, roomPrefab.Count)];
    }
    //Generate Map
    public void GenerateMap()
    {
        //Map of the Day
        if (mapOfTheDay)
        {
            DateTime dt = DateTime.Now;
            UnityEngine.Random.InitState(mapDayMthDay(dt));
        }
        //Random Map which is leave it alone
        else if (randomMap)
        {

        }
        //Seeded Map which means it was determine from the present seed
        else if (seededMap)
        {
            UnityEngine.Random.InitState(seed);
        }
        //Grid
        grid = new Room[cols, rows];
        
        Vector3 centerOffset = new Vector3(roomWidth * (cols - 1) * 0.5f, 0.0f, roomHeight * (rows - 1) * 0.5f);
        //Rows
        for (int i = 0; i < rows; i++)
        {
            //Columns
            for (int j = 0; j < cols; j++)
            {
                //X and Z for width and length for the location
                //XPosition
                float xPosition = roomWidth * j;
                //ZPosition
                float zPosition = roomHeight * i;
                //Vector 3
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);
                newPosition -= centerOffset;
                //Create the tiles
                CreateTile(newPosition, i, j);
                
            }
        }
    }
    //Create the Tiles (Doors, Walls, Barricade)
    void CreateTile(Vector3 position, int i, int j)
    {
        //Create and Spawn tiles using the Random Prefab
        GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), position, Quaternion.identity) as GameObject;

        tempRoomObj.transform.parent = this.transform;
        //Name of the spawned object
        tempRoomObj.name = "Room_" + j + "," + i;

        Room tempRoom = tempRoomObj.GetComponent<Room>();
        //Operate the doors
        OperateDoors(tempRoom, i, j);

        grid[j, i] = tempRoom;
    }
    //Operating doors
    void OperateDoors (Room room, int i, int j)
    {
        //****North and South Door****
        if (i == 0)
        {
            room.doorNorth.SetActive(false);
        }
        else if (i == rows - 1)
        {
            room.doorSouth.SetActive(false);
        }
        else
        {
            room.doorNorth.SetActive(false);
            room.doorSouth.SetActive(false);
        }
        //****North and South Door****
        //****East and West Door****
        if (j == 0)
        {
            room.doorEast.SetActive(false);
        }
        else if (j == cols - 1)
        {
            room.doorWest.SetActive(false);
        }
        else
        {
            room.doorEast.SetActive(false);
            room.doorWest.SetActive(false);
        }
        //****East and West Door****
    }
    //Map of the Day
    public int mapDayMthDay(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day;
    }
}
