﻿//Norman Nguyen
//Rooms Objects (mostly doors)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    //Door Objects
    public GameObject doorNorth;
    public GameObject doorSouth;
    public GameObject doorEast;
    public GameObject doorWest;
    //Powerup Spawn
    public GameObject PowerupSpawnPoint;

}
