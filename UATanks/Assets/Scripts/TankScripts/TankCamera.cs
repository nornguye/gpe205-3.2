﻿//Tank Camera: A camera that follows the tank as it moves and rotates.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCamera : MonoBehaviour
{
    public Transform player;
    //Offset the vector to allow the camera to not tie to the tank
    public Vector3 offset;
    //Turn Speed for the camera
    public float turnSpeed;
    public GameObject playerPrefab;
    //Moves the tank while updating on the position for the camera.
    void Update()
    {
        player = GameObject.Find("Tank(Clone)").transform;
        //Camera moves as it follows the cannon
        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offset;
        //Updating on the position of the character and camera
        transform.position = player.position + offset;
        //Positioning to look at the player
        transform.LookAt(player.transform);
    }
}

