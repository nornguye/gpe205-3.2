﻿//Norman Nguyen
//Tank Health: Data of tanks health
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankHealth : MonoBehaviour
{
    //Variables for health and damage
    public float currentHealth = 100;
    //Update on the health system.
    void Update()
    {
        //Destroy Tank if it hits to zero
        if (gameObject != null)
        {
            if (currentHealth == 0)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
