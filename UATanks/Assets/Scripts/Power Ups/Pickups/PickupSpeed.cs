﻿//Norman Nguyen
//Pick Up Speed
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpeed : MonoBehaviour {
    //Speed Powerup
    public PowerupSpeed powerup;


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
    //On Trigger event where the tank collides (REMEMBER TO TURN ON TRIGGER)
    private void OnTriggerEnter(Collider other)
    {
        //Link from the Powerup Manager for Get Component
        PowerupManager pm = other.GetComponent<PowerupManager>();

        if (pm != null)
        {
            //Add Powerup
            pm.AddPowerup(powerup);
            // Destroy this powerup
            Destroy(gameObject);
        }
    }
}
